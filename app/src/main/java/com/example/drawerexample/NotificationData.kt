package com.example.drawerexample

data class NotificationData(
    val title: String,
    val message: String
)