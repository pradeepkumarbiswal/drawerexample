package com.example.drawerexample.services

import com.example.drawerexample.models.MyCountry
import retrofit2.Call
import retrofit2.http.GET

interface CountryService {
    @GET("countries")
    fun getAffectedCountryList () : Call<List<MyCountry>>
}