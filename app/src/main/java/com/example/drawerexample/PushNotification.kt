package com.example.drawerexample

import com.example.drawerexample.NotificationData

data class PushNotification(
    val data: NotificationData,
    val to: String
)